package observability

import (
	"context"

	"github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"{{ .git.base }}/{{ .project.name }}/pkg/utils"
)

func CreateObserv(ctxIn context.Context, tracer trace.Tracer) (ctx context.Context, span trace.Span, log *logrus.Entry) {
	long, p, o, fc := utils.CallerName(1)
	ctx, span = tracer.Start(ctxIn, long)
	log = logrus.WithFields(
		logrus.Fields{"traceid": span.SpanContext().TraceID(), "pkg": p, "func": fc},
	)
	span.SetAttributes(attribute.String("pkg", p), attribute.String("func", fc))
	if o != "" {
		span.SetAttributes(attribute.String("obj", o))
		log = log.WithField("obj", o)
	}
	return
}
