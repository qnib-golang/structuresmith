package utils

import (
	"fmt"
	"regexp"
	"runtime"
)

var (
	reg = regexp.MustCompile(`.*\/(?P<pkg>\w+)\.(\((?P<obj>[\w\*]+)\)\.)?(?P<func>\w+)`)
)

func CallerName(skip int) (long, p, o, f string) {
	pc, _, _, ok := runtime.Caller(skip + 1)
	if !ok {
		return
	}
	fc := runtime.FuncForPC(pc)
	if fc == nil {
		return
	}
	return parseCallerPath(fc.Name())
}

func parseCallerPath(in string) (long, p, o, f string) {
	g, err := ReSubMatchMap(reg, in)
	if err != nil {
		return
	}
	p = g["pkg"]
	f = g["func"]
	long = fmt.Sprintf("%s.%s", p, f)
	if obj, ok := g["obj"]; ok && obj != "" {
		long = fmt.Sprintf("%s.%s", obj, f)
		o = obj
	}
	return long, g["pkg"], o, g["func"]
}
