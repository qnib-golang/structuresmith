package observability

import (
	"context"
	"os"
	"testing"

	"{{ .git.base }}/{{ .project.name }}/pkg/utils/cnt"
)

func TestSetupTracing(t *testing.T) {
	ctx := context.TODO()
	otC, err := cnt.StartOpentelemetryCollectorContainer(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := otC.Terminate(ctx); err != nil {
			panic(err)
		}
	}()

	port, err := otC.MappedPort(ctx, "4318")
	if err != nil {
		t.Fatal(err)
	}
	url := "http://localhost:" + port.Port()
	os.Setenv("OTEL_EXPORTER_OTLP_INSECURE", "true")
	shutdown, skip, err := SetupOTelSDK(ctx, "test", url)
	if err != nil {
		t.Fatal(err)
	}
	if skip {
		t.Skip("skipping test; no OTel SDK")
	}
	if e := shutdown(ctx); e != nil {
		t.Fatal(e)
	}
}
