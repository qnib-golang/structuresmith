/*
Copyright © {{ .year }} NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"{{ .git.base }}/{{ .project.name }}/pkg/lib/sleep"
)

// parseCmd represents the parse command
var sleepCmd = &cobra.Command{
	Use:   "sleep [duration] [duration] ...",
	Short: "Run a sleep command.",
	Long:  "Run a sleep command. This command will sleep for the duration specified in milliseconds. If no duration is specified, the command will sleep for 100 milliseconds.",
	Run: func(cmd *cobra.Command, args []string) {
		if err := sleep.RunCMD(cmd, args); err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(sleepCmd)
	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// parseCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// parseCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
