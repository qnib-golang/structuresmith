package cmd_test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"{{ .git.base }}/{{ .project.name }}/pkg/cmd"
)

var testVersion = "0.0.0"

func TestPrintVersion(t *testing.T) {
	gotV1 := cmd.PrintVersion(false)
	assert.Equal(t, "v"+testVersion+"\n", gotV1)
	gotV2 := cmd.PrintVersion(true)
	slVer := strings.Split(testVersion, ".")
	assert.Equal(t, fmt.Sprintf(`{"major":%s,"minor":%s,"patch":%s}`, slVer[0], slVer[1], slVer[2]), gotV2)

}
