package observability

import (
	"context"
	"testing"

	"go.opentelemetry.io/otel"
)

func TestCreateObeserver(t *testing.T) {
	ctx := context.Background()
	tracer := otel.Tracer("test-tracer")
	c, s, l := CreateObserv(ctx, tracer)
	if c == nil {
		t.Errorf("Expected observer to not be nil")
	}
	if s == nil {
		t.Errorf("Expected observer to not be nil")
	}
	if l == nil {
		t.Errorf("Expected observer to not be nil")
	}
}
