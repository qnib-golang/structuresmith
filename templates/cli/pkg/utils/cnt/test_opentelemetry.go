package cnt

import (
	"context"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

func StartOpentelemetryCollectorContainer(ctx context.Context) (c testcontainers.Container, err error) {
	req := testcontainers.ContainerRequest{
		Image: "otel/opentelemetry-collector:0.93.0",
		ExposedPorts: []string{
			"4317/tcp",
			"4318/tcp",
			"55679/tcp",
		},
		WaitingFor: wait.ForListeningPort("4317").WithStartupTimeout(10 * time.Second),
	}
	if c, err = testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	}); err != nil {
		return c, err
	}
	return c, nil
}
