#!/bin/bash

docker buildx build -t {{ .oci.base }}/{{ .project.name }}:$(date +%F)-amd64 \
    --load --platform=linux/amd64 --ssh default=$SSH_AUTH_SOCK .
docker buildx build -t {{ .oci.base }}/{{ .project.name }}:$(date +%F)-arm64 \
    --load --platform=linux/arm64 --ssh default=$SSH_AUTH_SOCK .

docker manifest create \
    {{ .oci.base }}/{{ .project.name }}:$(date +%F) \
    {{ .oci.base }}/{{ .project.name }}:$(date +%F)-arm64 \
    {{ .oci.base }}/{{ .project.name }}:$(date +%F)-amd64

docker manifest push \
    {{ .oci.base }}/{{ .project.name }}:$(date +%F)
