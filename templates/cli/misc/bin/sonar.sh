#!/bin/bash

if [[ -z "${SONAR_QUBE_PROJECT}" ]];then
  echo "SONAR_QUBE_PROJECT is not set"
  echo "Please set the environment variable SONAR_QUBE_PROJECT,SONAR_QUBE_URL and SONAR_QUBE_TOKEN"
  exit 1
fi
#if [[ "$1" == "local" ]];then
#  SONAR_QUBE_URL=${SONAR_QUBE_URL_LOCAL}
#  SONAR_QUBE_TOKEN=${SONAR_QUBE_TOKEN_LOCAL}
#fi

set -x 

sonar-scanner \
  -Dsonar.branch.name=$(git rev-parse --abbrev-ref HEAD) \
  -Dsonar.projectKey=${SONAR_QUBE_PROJECT} \
  -Dsonar.sources=. \
  -Dsonar.host.url=${SONAR_QUBE_URL} \
  -Dsonar.login=${SONAR_QUBE_TOKEN}
