# {{ .project.name }}

{{ .project.description }}

## Setup Environment

```bash
docker-compose up -d
```

## Run Tests

```bash
bash ./misc/bin/test.sh
bash ./misc/bin/sonar.sh
```
