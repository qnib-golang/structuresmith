/*
Copyright © {{ .year }} Christian Kniep <christian@qnib.org>
*/
package main

import "{{ .git.base }}/{{ .project.name }}/pkg/cmd"

func main() {
	cmd.Execute()
}
