package sleep

import (
	"fmt"
	"strconv"
	"time"

	logrusloki "github.com/schoentoon/logrus-loki"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"

	"{{ .git.base }}/{{ .project.name }}/pkg/lib/observability"
)

type Sleep struct {
	durations []int
}

var tracer = otel.Tracer("sleep")

func New(args ...string) *Sleep {
	s := Sleep{}
	if len(args) == 0 {
		args = append(args, "100")
	}
	for _, a := range args {
		i, err := strconv.Atoi(a)
		if err != nil {
			logrus.Errorf("Failed to convert %s to int: %v", a, err)
			continue
		}
		s.durations = append(s.durations, i)
	}
	return &s
}

func RunCMD(cmd *cobra.Command, args []string) (err error) {
	ctx := cmd.Context()
	var span trace.Span
	//utils.ViperBindFlags(cmd)
	otelEP := viper.GetString("otel.endpoint")
	if shutdown, skip, err := observability.SetupOTelSDK(ctx, "csv", otelEP); err != nil {
		return err
	} else if !skip {
		defer shutdown(ctx)
		ctx, span = tracer.Start(ctx, "csvCmd.Run")
		defer span.End()
	}

	lokiEP := viper.GetString("loki.endpoint")
	if lokiEP != "" {
		logrus.Infof("Setting up Loki with endpoint: %s", lokiEP)
		hook, err := logrusloki.NewLokiDefaults(fmt.Sprintf("%s/loki/api/v1/push", lokiEP))
		if err != nil {
			return err
		}
		logrus.AddHook(hook)
	}
	s := New(args...)
	err = s.Run()
	if err != nil {
		logrus.Error(err)
	}
	return
}

func (s *Sleep) Run() (err error) {
	for _, d := range s.durations {
		logrus.Infof("Sleeping for %dms", d)
		time.Sleep(time.Duration(d) * time.Millisecond)
	}
	return
}
