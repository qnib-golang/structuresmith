package utils

import "testing"

func TestParseCallerPath(t *testing.T) {
	tests := []struct {
		in   string
		long string
	}{
		{`github.com/test/pkg/mypkg.NewCMD`, `mypkg.NewCMD`},
		{`github.com/test/pkg/lib/obj.(*Blob).getSomething`, `*Blob.getSomething`},
	}
	for _, test := range tests {
		t.Run(test.long, func(t *testing.T) {
			long, _, _, _ := parseCallerPath(test.in)
			if long != test.long {
				t.Errorf("Expected %s, got %s", test.long, long)
			}
		})

	}

}
