package cnt

import (
	"context"
	"io"
	"os"
	"path"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

func StartSshContainer(ctx context.Context, tempD string) (c testcontainers.Container, rsaFile string, err error) {
	req := testcontainers.ContainerRequest{
		Image:        "registry.gitlab.com/qnib-pub-containers/testcontainers/sshd:2024-01-23.1",
		ExposedPorts: []string{"22/tcp"},
		WaitingFor:   wait.ForListeningPort("22"),
	}
	c, err = testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return c, rsaFile, err
	}
	rsaFile = path.Join(tempD, "id_rsa")
	if r, err := c.CopyFileFromContainer(ctx, "/root/.ssh/id_rsa"); err != nil {
		return c, rsaFile, err
	} else if outFile, err := os.Create(rsaFile); err != nil {
		return c, rsaFile, err
	} else {
		// handle err
		defer outFile.Close()
		if _, err = io.Copy(outFile, r); err != nil {
			return c, rsaFile, err
		}
	}
	return c, rsaFile, err
}
